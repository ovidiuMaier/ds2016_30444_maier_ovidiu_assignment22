package serviceInterfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

import entities.Car;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-two-common-classes
 * @Since: Sep 1, 2015
 * @Description:
 * 	Interface of the remote object published by the server. It allows for the computation
 * 	of the tax needed to be payed for a Car.
 */
public interface ISellingPrice extends Remote{

	/**
	 * Computes the price to be payed for a Car.
	 *
	 * @param c Car for which to compute the tax
	 * @return tax for the car
	 */
	double computeSellingPrice(Car c) throws RemoteException;
}