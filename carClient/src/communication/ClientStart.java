package communication;

import java.math.BigDecimal;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import controllers.CatalogController;
import entities.Car;
import serviceInterfaces.ITaxService;
import serviceInterfaces.ISellingPrice;

public class ClientStart {
    public static void main(String args[]) {
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        try {
//            String name = "ITaxService";
//            Registry registry = LocateRegistry.getRegistry();
//            ITaxService taxService = (ITaxService) registry.lookup(name);
//            
//            System.out.println("Tax value: "
//					+ taxService.computeTax(new Car(2009, 2000, 1000)));
//       
//            String name2 = "ISellingPrice";
//            ISellingPrice sellingPrice = (ISellingPrice) registry.lookup(name2);
//            
//            System.out.println("Price: " + sellingPrice.computeSellingPrice(new Car(2009, 2000, 1000)));
        	  
        	new CatalogController();            

        } catch (Exception e) {
            System.err.println("ComputePi exception:");
            e.printStackTrace();
        }
    }    
}