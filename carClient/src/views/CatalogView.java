package views;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description:
 *	CatalogView is a JFrame which contains the UI elements of the Client application.
 */
public class CatalogView extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField year;
	private JTextField engineSize;
	private JTextField price;
	private JButton btnPrice;
	private JButton btnTax;
	private JTextArea textArea;

	public CatalogView() {
		setTitle("Car");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(300, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblInsertNewStudent = new JLabel("Car");
		lblInsertNewStudent.setBounds(10, 11, 120, 14);
		contentPane.add(lblInsertNewStudent);

		JLabel lblFirstname = new JLabel("Year");
		lblFirstname.setBounds(10, 36, 60, 14);
		contentPane.add(lblFirstname);

		JLabel lblLastname = new JLabel("Engine");
		lblLastname.setBounds(10, 61, 60, 14);
		contentPane.add(lblLastname);

		JLabel lblMail = new JLabel("Price");
		lblMail.setBounds(10, 86, 46, 14);
		contentPane.add(lblMail);

		year = new JTextField();
		year.setBounds(80, 33, 86, 20);
		contentPane.add(year);
		year.setColumns(10);

		engineSize = new JTextField();
		engineSize.setBounds(80, 58, 86, 20);
		contentPane.add(engineSize);
		engineSize.setColumns(10);

		price = new JTextField();
		price.setBounds(80, 83, 86, 20);
		contentPane.add(price);
		price.setColumns(10);

		btnTax = new JButton("Tax");
		btnTax.setBounds(10, 132, 89, 23);
		contentPane.add(btnTax);

		
		btnPrice = new JButton("Price");
		btnPrice.setBounds(235, 77, 89, 23);
		contentPane.add(btnPrice);

		textArea = new JTextArea();
		textArea.setBounds(235, 131, 171, 120);
		contentPane.add(textArea);
	}

	public void addBtnPriceActionListener(ActionListener e) {
		btnPrice.addActionListener(e);
	}

	public void addBtnTaxActionListener(ActionListener e) {
		btnTax.addActionListener(e);
	}

	public String getYear() {
		return year.getText();
	}

	public String getEngineSize() {
		return engineSize.getText();
	}

	public String getPrice() {
		return price.getText();
	}

	public void printResult(String result) {
		textArea.setText(result);
	}

	public void clear() {
		year.setText("");
		engineSize.setText("");
		price.setText("");
	}
}
