package controllers;

import views.CatalogView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import serviceInterfaces.ITaxService;
import serviceInterfaces.ISellingPrice;

import entities.Car;

public class CatalogController {
private CatalogView catalogView;

public CatalogController() {
	catalogView = new CatalogView();
	catalogView.setVisible(true);
	
	catalogView.addBtnTaxActionListener(new TaxActionListener());
	catalogView.addBtnPriceActionListener(new PriceActionListener());
}


public void displayErrorMessage(String message) {
	catalogView.clear();
	JOptionPane.showMessageDialog(catalogView, message, "Error", JOptionPane.ERROR_MESSAGE);
}

public void displayInfoMessage(String message) {
	catalogView.clear();
	JOptionPane.showMessageDialog(catalogView, message, "Success", JOptionPane.PLAIN_MESSAGE);
}

class TaxActionListener implements ActionListener {
	@Override
	public void actionPerformed(ActionEvent e) {
		String year = catalogView.getYear();
		String engineSize = catalogView.getEngineSize();
		String price = catalogView.getPrice();
		try {
      String name = "ITaxService";
      Registry registry = LocateRegistry.getRegistry();
      ITaxService taxService = (ITaxService) registry.lookup(name);
      
      
      double res = taxService.computeTax(new Car(Integer.parseInt(year), Integer.parseInt(engineSize), Integer.parseInt(price)));
      
      String string_res = String.valueOf(res);
      
       catalogView.printResult(string_res);

		} catch (Exception exc) {
            System.err.println("ComputePi exception:");
            exc.printStackTrace();
        }
      
		}
}

class PriceActionListener implements ActionListener {
	@Override
	public void actionPerformed(ActionEvent e) {
		String year = catalogView.getYear();
		String engineSize = catalogView.getEngineSize();
		String price = catalogView.getPrice();
		try {
      String name = "ISellingPrice";
      Registry registry = LocateRegistry.getRegistry();
      ISellingPrice sellingPrice = (ISellingPrice) registry.lookup(name);
      
      
      double res = sellingPrice.computeSellingPrice(new Car(Integer.parseInt(year), Integer.parseInt(engineSize), Integer.parseInt(price)));
      
      String string_res = String.valueOf(res);
      
       catalogView.printResult(string_res);

		} catch (Exception exc) {
            System.err.println("ComputePi exception:");
            exc.printStackTrace();
        }
      
		}
}
	



}
