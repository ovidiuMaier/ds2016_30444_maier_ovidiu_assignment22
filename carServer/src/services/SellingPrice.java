package services;


import entities.Car;
import serviceInterfaces.ISellingPrice;


/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-two-server
 * @Since: Sep 1, 2015
 * @Description:
 * 	Class used for computation of taxes to be paid for a specific car. An instance
 * 	of this class is published in the Registry so that it can be remotely accessed
 * 	by a client.
 */
public class SellingPrice implements ISellingPrice {

	public double computeSellingPrice(Car c) {
		// Dummy formula
		
		double purchassingPrice = c.getPrice();
		if (purchassingPrice <= 0) {
			throw new IllegalArgumentException("Price must be positive.");
		}
		double sellingPrice = purchassingPrice -( (purchassingPrice / 7) * (2015 - c.getYear()) ); 
		return sellingPrice;
	}
}
