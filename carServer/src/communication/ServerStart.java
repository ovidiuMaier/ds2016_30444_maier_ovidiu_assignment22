package communication;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import serviceInterfaces.ITaxService;
import serviceInterfaces.ISellingPrice;
import services.TaxService;
import services.SellingPrice;

public class ServerStart {

    public static void main(String[] args) {
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        try {
            String name = "ITaxService";
            ITaxService taxService = new TaxService();
            ITaxService stub =
                (ITaxService) UnicastRemoteObject.exportObject(taxService, 0);
            Registry registry = LocateRegistry.getRegistry();
            registry.rebind(name, stub);
            
            String name2 = "ISellingPrice";
            ISellingPrice sellingPrice = new SellingPrice();
            ISellingPrice stub2 =
                    (ISellingPrice) UnicastRemoteObject.exportObject(sellingPrice, 0);
            registry.rebind(name2, stub2);
            
            System.out.println("ComputeEngine bound");
        } catch (Exception e) {
            System.err.println("ComputeEngine exception:");
            e.printStackTrace();
        }
    }
}
